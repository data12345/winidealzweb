import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { LocalStorage } from 'angular-web-storage';
import { CommonService } from 'src/app/services/common.service';
import { NgForm } from '@angular/forms';
declare var $: any;

@Component({
  selector: 'app-cartpage',
  templateUrl: './cartpage.component.html',
  styleUrls: ['./cartpage.component.scss']
})
export class CartpageComponent implements OnInit {
  
  cardData: any;
  loyalty_can_be: any;
  cartList: any;
  donateStatus: boolean = false
  warehouse:boolean=false
  quantity: any;
  loyality: number;
  shipping_charges: number=0;
  tax_percentage: any;
  status: any
  status1: any;
  grandTotal:number=0
  price: any;
  shipping:boolean=false;
  authtoken: any
  order_name: any;
  order_number: any;
  postal_code: any;
  city: any;
  apartment: any;
  building_name: any;
  address: boolean = false
  userAddress: string;
  apply:boolean=true
  promo_code_name: string = '';
  campign_id: any;
  discount_type: any = '';
  discount_percent: number = 0;
  campign_count: any;
  campaign: { campaign_id: any; campaign_count: any; }[];
  donate_status: number = 1;
  hq_status: number = 0;
  total: number;
  remove:boolean=false;
  Donateticket_count: number;
  notdonateticket_count: any;
  ticket_count: number;
  hq_address: any;
  spinner: boolean = false;
  addressStatus :boolean=true
  charges: number;
  body: { campaign: { campaign_id: any; }[]; promo_code_name: any; };
  canelCartData: any;
  indexCancel: any;
  country_code_address: any;
  country: any;
  countryCode: any;
  countryStatusAddress: boolean = true;
  config1: {};
  subTotal: number = 0;

  constructor(private router: Router, public service: ApiService, private toastr: ToastrService, public comm: CommonService) { }

  ngOnInit() {
    this.grandTotal=0;
    this.subTotal=0;
    this.donateStatus=false
    window.scroll(0, 0);
    //this.discount_percent = 0
    this.status = 1
    localStorage.setItem(
      "donate_status",
      JSON.stringify(this.donate_status)
    );
    if(localStorage.getItem('discount_percent')){
      this.discount_percent = (JSON.parse(localStorage.getItem('discount_percent')));
    }else{
      localStorage.setItem(
        "discount_percent",
        JSON.stringify(0)
      );
    }

    if(localStorage.getItem('discount_type')){
      this.discount_type = (JSON.parse(localStorage.getItem('discount_type')));
    }else{
      localStorage.setItem(
        "discount_type",
        JSON.stringify('')
      );
    }
    localStorage.setItem(
      "hq_status",
      JSON.stringify(this.hq_status)
    );

    if(localStorage.getItem('promo_code_name')){
      this.promo_code_name = (JSON.parse(localStorage.getItem('promo_code_name')));
      if(this.promo_code_name != ''){
        this.apply = false;
        this.remove = true;
      }
    }else{
      localStorage.setItem(
        "promo_code_name",
        JSON.stringify('')
      );
    }
    localStorage.setItem(
      "promo_code_status",
      JSON.stringify('0')
    );


    this.service.getCountryCode().subscribe((res: any) => {
      this.countryCode = res.countryArray
      console.log(this.countryCode.length, res.countryArray.length)
      this.config1 = {
        displayKey: "Code", //if objects array passed which key to be displayed defaults to description
        search: true, //true/false for the search functionlity defaults to false,
        height: "150px", //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
        placeholder: "Code", // text to be displayed when no item is selected defaults to Select,
        customComparator: () => { }, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
        limitTo: this.countryCode.length, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
        moreText: "more", // text to be displayed whenmore than one items are selected like Option 1 + 5 more
        noResultsFound: "No results found!", // text to be displayed when no items are found while searching
        searchPlaceholder: "Search", // label thats displayed in search input,
        searchOnKey: "Code" // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
      };
    });
    if (localStorage.getItem('sigUp')) {
      this.order_name = JSON.parse(localStorage.getItem('sigUp')).order_name
      this.order_number = JSON.parse(localStorage.getItem('sigUp')).order_number
      this.postal_code = JSON.parse(localStorage.getItem('sigUp')).postal_code
      this.city = JSON.parse(localStorage.getItem('sigUp')).city
      this.apartment = JSON.parse(localStorage.getItem('sigUp')).apartment
      this.building_name = JSON.parse(localStorage.getItem('sigUp')).building_name
      this.country_code_address = JSON.parse(localStorage.getItem('sigUp')).country_code
      this.country = JSON.parse(localStorage.getItem('sigUp')).country
      if(this.apartment=="" || this.city==''){
        this.userAddress=''
      }
      else{
      this.userAddress = this.city + '-' + this.postal_code + '\n' + this.building_name + '\n' +this.apartment 
      }
      console.log(this.userAddress)
    }
    $('.cart_donate_btn button').click(function () {
      $(this).addClass('donate_active').siblings().removeClass('donate_active');
    });
    $('.dont_donate_btn').click(function () {
      $('.address_select_sec').css('display', 'block')
    })
    $('.donate_btn').click(function () {
      $('.address_select_sec').css('display', 'block')
    })
    $('.address_btn_cart button').click(function () {
      $('.cart_add_address').css('display', 'block')
    })
    $('.close_btn_address').click(function () {
      $('.cart_add_address').css('display', 'block')
    })
    this.getSetting()
    if (localStorage.getItem("campaign")) {
      this.cartList = JSON.parse(localStorage.getItem("campaign"));
    }
    if (localStorage.getItem('auth_token')) {
      this.authtoken = localStorage.getItem("auth_token")
      this.getCartDetails();
    }
    else {
      this.getUsercart();
    }
  }
  gotoLogin() {
    this.router.navigate(['/login'])
  }
  goTopaynow() {
   this.donateStatus = false;
   this.warehouse = false;
    this.donate_status = 1
    localStorage.setItem(
      "donate_status",
      JSON.stringify(this.donate_status)
    );
     let element = document.querySelector(".donate_active");
    if (element) {
      element.classList.remove("donate_active");
      // element.classList.add("inative");
    }
    document.getElementById('donate_default').classList.add("donate_active");
  }
  goTopaynowDATA() {
  this.router.navigate(['/paynow']);
  }
  getSetting() {
    this.service.getSettingData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.loyality = res.data.loyalty.earned_on_purchases
        this.charges = parseInt(res.data.setting.shipping_charges)
        this.tax_percentage = res.data.setting.tax_percentage
        this.hq_address=res.data.setting.hq_address

      }
    }
    )
  }
  addcoupon(form: NgForm) {
    var id= [];
    if (localStorage.getItem('auth_token')) {
      this.cardData.forEach(element => {
        console.log(element.campaign_id)
        var data = {
          "campaign_id":element.campaign_id 
        }
        id.push(data)
        this.campign_id=element.campaign_id
        this.body = {
          "campaign": id,
          "promo_code_name": form.value.promo_code_name
        }
      })
    }    
    else{
      this.cardData.forEach(element => {
        console.log(element.id)
        var data = {
          "campaign_id":element.campaign_id 
        }
        id.push(data)
        this.campign_id=element.id
        this.body = {
          "campaign": id,
          "promo_code_name": form.value.promo_code_name
        }
      })
    }
    console.log(form, form.value.promo_code_name)
    if (form.value.promo_code_name == "" || form.value.promo_code_name == undefined) {
      localStorage.setItem(
        "promo_code_name",
        JSON.stringify(this.promo_code_name)
      );
      localStorage.setItem(
        "promo_code_status",
        JSON.stringify('1')
      );
      this.toastr.error('Enter Promo code', "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
    }
    else {

      this.service.applyPromoCode(this.body).subscribe((res: any) => {
        if (res.status == 1) {
          this.discount_type = res.data.discount_type;
          localStorage.setItem(
            "promo_code_name",
            JSON.stringify(this.promo_code_name)
          );
          localStorage.setItem(
            "discount_type",
            JSON.stringify(this.discount_type)
          );
          this.remove=true
          this.apply=false
          this.discount_percent = res.data.discount_percent

          this.grandTotal =(Number(this.shipping_charges) + (Number(this.subTotal) - Number(this.subTotal) * Number((this.discount_percent) / 100)));

          localStorage.setItem(
            "discount_percent",
            JSON.stringify(this.discount_percent)
          );
          localStorage.setItem(
            "total",
            JSON.stringify(this.grandTotal)
          );
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });

      }
      )
    }

  }
  selectedcancelList(i,item){
    this.canelCartData=item,
    this.indexCancel=i
    console.log(i,item)
  }
  getCartDetails() {
    // this.spinner = true;
    this.service.getCartData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.cardData = res.data.cart
        this.cardData.forEach(element => {
          element.itemprice=element.campaign.product.price * element.campaign_count
          this.subTotal= this.subTotal + element.itemprice;
          this.grandTotal= this.grandTotal + element.itemprice;
          this.grandTotal =  (Number(this.shipping_charges) + (Number(this.subTotal) - Number(this.subTotal) * Number((this.discount_percent) / 100)));

          // this.notdonateticket_count = element.campaign.ticket_count
          this.Donateticket_count = Number(element.campaign.ticket_count) + Number(element.campaign.donate_ticket_count)
          if(element.campaign.early_item_number > element.campaign.inventory_sold)
          element.count=Number(element.campaign.early_ticket_count ) +  Number(element.campaign.donate_ticket_count)
          else{
          element.count = this.Donateticket_count}
          this.campign_id = element.campaign_id
          this.campign_count = element.campaign_count
          this.price = element.campaign.product.price * element.campaign_count
          localStorage.setItem(
            "total",
            JSON.stringify(this.grandTotal)
          );
        });
        this.spinner=false;
        var campaign = {
          'campaign': [
            {
              "campaign_id": this.campign_id, "campaign_count": this.campign_count,
            }
          ]
        }
        localStorage.setItem(
          "campaignData",
          JSON.stringify(campaign)
        );
        this.loyalty_can_be = res.data.loyalty_can_be
        var count = 0;
        for(let item of this.cardData){
          count = count + item.campaign_count 
        }
        this.comm.setAuthgetCardValue(count);

        localStorage.removeItem("campaign");
        localStorage.removeItem("campaignslength");
      }
    }
    )
  }

  submit(){
    if(JSON.parse(localStorage.getItem('donate_status'))== 0 && this.status == false){
      if(this.apartment=="" || this.city==''){
        return this.toastr.error('Enter your address', "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      }
      else{
        if(JSON.parse(localStorage.getItem('donate_status'))== 0)
        $('#proceed_pay').modal('show');
      }
    }else if(JSON.parse(localStorage.getItem('donate_status'))== 0 && this.status){
      if(JSON.parse(localStorage.getItem('donate_status'))== 0)
      $('#proceed_pay').modal('show');
    }else if(JSON.parse(localStorage.getItem('donate_status'))== 1 && localStorage.getItem('auth_token')){
      this.router.navigate(['paynow'])
    }else if(JSON.parse(localStorage.getItem('donate_status'))== 1 && !this.authtoken){
      $('#Continue_login').modal('show');
    }
    localStorage.removeItem('discount_type');
    localStorage.removeItem('promo_code_name');
  }

  changeToggle() {
    this.address = false
    this.warehouse=true
    this.shipping_charges=0;
    this.shipping=false;
    this.status = !this.status;
    if (!this.status == true) {
      this.hq_status = 0
      this.shipping=true
      this.shipping_charges=this.charges
      this.grandTotal = (Number(this.shipping_charges) + (Number(this.subTotal) - Number(this.subTotal) * Number((this.discount_percent) / 100)));
      localStorage.setItem(
        "total",
        JSON.stringify(this.grandTotal)
      );

      this.warehouse=false
      this.address = true
      localStorage.setItem(
        "hq_status",
        JSON.stringify(this.hq_status)
      );
      if(!this.userAddress){
      this.onManageAddress();}
    }
    else {
      this.shipping_charges=0
       this.hq_status = 1
       this.grandTotal = (Number(this.shipping_charges) + (Number(this.subTotal) - Number(this.subTotal) * Number((this.discount_percent) / 100)));
      localStorage.setItem(
        "hq_status",
        JSON.stringify(this.hq_status)
      );
    }
  }
  gotoAddress() {
    this.onManageAddress();
  }
  decrement(data) {
    if (localStorage.getItem('auth_token')) {
      data.campaign_count--;
      this.changequantity(data);
    }
    else {
      data.cart_count--;
      this.changequantity(data);
    }
  }
  removeItem(){
    this.cancel(this.indexCancel,this.canelCartData)
  }
  cancel(index, data) {
    if (localStorage.getItem('auth_token')) {
      console.log(data)
      this.service.removeFromCart(data.campaign_id).subscribe((res: any) => {
        console.log(res);
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.ngOnInit()
        }
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      }
      )
    }
    else {
      this.grandTotal = 0;
      this.subTotal = 0;
      this.cardData.splice(index, 1);
      const i = _.findIndex(this.cartList, {
        campaign_id: data.id,
      });
      if (i > -1) {
        this.cartList.splice(i, 1);
        
        this.cardData.forEach(ele =>{
          this.cartList.forEach(list =>{
            if(ele.id == list.campaign_id){
              this.subTotal = this.subTotal + ele.itemprice;
              this.grandTotal = this.grandTotal + ele.itemprice;
            }
          })
        })
        this.grandTotal =  (Number(this.shipping_charges) + (Number(this.subTotal) - Number(this.subTotal) * Number((this.discount_percent) / 100)));
        const data: any = localStorage.setItem(
          "campaign",
          JSON.stringify(this.cartList)
        );
        var count = 0;
        for(let item of this.cartList){
          count = count + item.campaign_count 
        }
        localStorage.setItem(
          "campaignslength",
          JSON.stringify(count)
        );
        if(localStorage.getItem('campaignslength')){
          if(JSON.parse(localStorage.getItem('campaignslength')) == 0){
            localStorage.removeItem('discount_percent');
            localStorage.removeItem('discount_type');
            localStorage.removeItem('promo_code_name');
          }
        }
      }
      this.comm.setgetCardValue();
    }
  }

  changequantity(data) {
    if (localStorage.getItem('auth_token')) {
      this.quantity = data.campaign_count;
      this.price = data.campaign.product.price * this.quantity;
      data.itemprice=data.campaign.product.price * this.quantity
      var campaign = {
        "campaign_id": data.campaign_id,
        "campaign_count": this.quantity
      }
      console.log(this.quantity,"----------");
      let Data = new FormData();
      Data.append('campaign_id', data.campaign_id);
      Data.append('campaign_count', this.quantity);
      this.service.addToCart(Data).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.ngOnInit()
        }else{
          this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
      }
      )
      if (this.quantity > data.campaign.max_per_user) {
        this.toastr.warning("You can not add more items.");
      } else if (this.quantity > (data.campaign.inventory - data.campaign.inventory_sold)) {
        this.toastr.warning("More items are not available.");
      }
    }
    else {
      this.quantity = data.cart_count;
      this.price = Number(data.product.price) * Number(data.cart_count);
      data.itemprice=data.product.price * this.quantity
      let product: any;
      product = JSON.parse(localStorage.getItem("campaign")) || []
      var item = product.find(
        item => item.campaign_id === data.id
      );
      if (item){
        item.campaign_id = item.campaign_id,
        item.campaign_count = this.quantity
        this.price = Number(data.product.price) * Number(item.campaign_count);
      }
      var count = 0;
      for(let item of product){
        count = count + item.campaign_count 
      }
      localStorage.setItem("campaign", JSON.stringify(product));
      localStorage.setItem("campaignslength", JSON.stringify(count));
      this.comm.setgetCardValue();
      this.ngOnInit();
    }
  }

  increment(data) {
    if (localStorage.getItem('auth_token')) {
      data.campaign_count++;
      this.changequantity(data);
    }
    else {
      console.log(data)
      data.cart_count++;
      this.changequantity(data);
    }
  }
  removeCoupon(){
    this.promo_code_name=''
    this.apply=true
    this.remove=false;
    this.discount_percent=0;
    localStorage.removeItem('discount_percent');
    localStorage.removeItem('discount_type');
    localStorage.removeItem('promo_code_name');
    this.ngOnInit()
  }
  getUsercart() {
    this.service.getGuestUsercart({ campaign: JSON.parse(localStorage.getItem("campaign")) }).subscribe((res: any) => {
      if (res.status == 1) {
        this.cardData = res.data
        this.cardData.forEach(ele => {
          this.campign_id = ele.id
          this.Donateticket_count = Number(ele.ticket_count) + Number(ele.donate_ticket_count)
          ele.itemprice=ele.product.price * ele.cart_count
          this.subTotal = this.subTotal +ele.itemprice
          this.grandTotal= this.grandTotal + ele.itemprice
          if(ele.early_item_number > ele.inventory_sold)
          ele.count =Number(ele.early_ticket_count ) +  Number(ele.donate_ticket_count)
          else{
          ele.count = this.Donateticket_count
        }
         this.loyalty_can_be = Number(ele.cart_count) * Number(this.loyality)
          this.price = Number(ele.product.price) * Number(ele.cart_count)
          this.grandTotal =  (Number(this.shipping_charges) + (Number(this.subTotal) - Number(this.subTotal) * Number((this.discount_percent) / 100)));
          this.spinner = false;
          localStorage.setItem(
            "total",
            JSON.stringify(this.grandTotal)
          );
        })
        this.comm.setAuthgetCardValue(this.cardData.length);
      }
    }
    )
  }

  gotohome() {
    this.router.navigate(['/homepage'])
  }

  donatenot(event) {
    this.donateStatus = true
    this.warehouse=true
    this.hq_status = 1
    this.ticket_count = this.notdonateticket_count
    this.donate_status = 0
    if(localStorage.getItem('auth_token')){
    this.cardData.forEach(element => {
      if(element.campaign.early_item_number>element.campaign.inventory_sold){
        element.count=element.early_ticket_count
      }else{
        element.count=element.campaign.ticket_count
      }
      this.notdonateticket_count = element.campaign.ticket_count
      element.count= this.notdonateticket_count
    })
  }
  else{
    this.cardData.forEach(element => {
    if(element.early_item_number>element.inventory_sold){
      element.count=element.early_ticket_count
    }else{
      element.count=element.ticket_count
    }
    this.notdonateticket_count = element.ticket_count
    element.count= this.notdonateticket_count
  })
    }
    localStorage.setItem(
      "donate_status",
      JSON.stringify(this.donate_status)
    );
    localStorage.setItem(
      "hq_status",
      JSON.stringify(this.hq_status)
    );
    let element = document.querySelector(".donate_active");
    console.log(element, event.target);
    if (element) {
      element.classList.remove("donate_active");
    }
    event.target.classList.add("donate_active");
  }

  donate(event) {
    this.donateStatus = false
    this.warehouse=false
    this.donate_status = 1
    this.hq_status = 0
    this.shipping_charges = 0;
    this.shipping = false;
    if(localStorage.getItem('auth_token')){
      this.cardData.forEach(element => {
        this.Donateticket_count = Number(element.campaign.ticket_count) + Number(element.campaign.donate_ticket_count)
        if(element.campaign.early_item_number > element.campaign.inventory_sold)
          element.count=Number(element.campaign.early_ticket_count ) +  Number(element.campaign.donate_ticket_count)
        else{
          element.count = this.Donateticket_count
        }
      })
    }
    else{
      this.cardData.forEach(ele => {
        this.Donateticket_count = Number(ele.ticket_count) + Number(ele.donate_ticket_count)
        if(ele.early_item_number > ele.inventory_sold)
          ele.count=Number(ele.early_ticket_count ) +  Number(ele.donate_ticket_count)
        else{
          ele.count = this.Donateticket_count
        }
      })
    }
    this.ticket_count = this.Donateticket_count
    localStorage.setItem(
      "donate_status",
      JSON.stringify(this.donate_status)
    );
    localStorage.setItem(
      "hq_status",
      JSON.stringify(this.hq_status)
    );
    let element = document.querySelector(".donate_active");
    if (element) {
      element.classList.remove("donate_active");
      element.classList.add("inative");
    }
    event.target.classList.add("donate_active");
    this.grandTotal =  (Number(this.shipping_charges) + (Number(this.subTotal) - Number(this.subTotal) * Number((this.discount_percent) / 100)));
    this.spinner = false;
    localStorage.setItem(
      "total",
      JSON.stringify(this.grandTotal)
    );
  }

  updateAddress(form:NgForm){
    let Data = new FormData();
    for (let key in form.value) {
      Data.append(`${key}`, form.value[key]);
    }
    this.service.addAddress(Data).subscribe((res: any) => {
      if (res.status == 1) {
        this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        localStorage.setItem("sigUp", JSON.stringify(res.data));
      }
      else {
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      }
    });
  }

  onManageAddress(){
    this.comm.addAddress().subscribe(res=>{
      if(res != undefined){
        this.apartment = res['apartment'];
        this.building_name = res['building_name'];
        this.city = res['city'];
        if(this.apartment=="" || this.city==''){
          this.userAddress=''
        }
        else{
          this.userAddress = this.city + '-' + this.postal_code + '\n' + this.building_name + '\n' +this.apartment 
        }
      }
    })
  }
}
