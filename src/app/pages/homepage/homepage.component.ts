import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { ToastrService } from 'ngx-toastr';
import * as $ from 'jquery';
import { CommonService } from 'src/app/services/common.service';
declare var slick: any;
import * as _ from "lodash";

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.scss']
})
export class HomepageComponent implements OnInit {
  slideConfig = {
    slidesToShow: 1, slidesToScroll: 1, autoplay: true,
    autoplaySpeed: 3000, dots: true,arrows: false, 
  };
  discount_percent: any;
  top_banner: any;
  all_campaigns: any;
  closing_campaigns: any;
  products: any;
  charity: any;
  tax: any;
  favourite: any;
  data: any;
  wishlist: any;
  pendingItem: number =1;
  cardItem: any;
  campaign_id: any;
  campaign_count: any;
  max_user: any;
  total:any
  fav: any;
  winners: any;
  cartList: any[];
  constructor(private router: Router, public service: ApiService, private toastr: ToastrService, public comm: CommonService) { }

  ngOnInit() {
    this.getHome()
    /****start slider */
    setTimeout(() => {
      this.script()
    }, 500);
    if (localStorage.getItem("campaign")) {
      this.cartList = JSON.parse(localStorage.getItem("campaign"));
    }
  }
  addScript(index: number, total: number) {
    console.log({
      index: index, total: total
    })
    if (this.total > 0 || this.total >= 30) {
      // alert(0)
      $('.product_progress_height').css({ 'height': '40%', 'background-color': '#0db14b	' })
    } else if (this.total > 30 || this.total <= 40) {
      // alert(1)
      $('.product_progress_height').css({ 'height': '30%', 'background-color': '#fcb83f' })
    } else {
      // alert(2)
      $('.product_progress_height').attr('style', 'height: 80%; background-color: #f44336')
    // } else if (total >= 60) {
    //   $('.product_progress_height').css({ 'height': '70%', 'background-color': '#FCB83F' })
    // } else if (total >= 50) {
    //   $('.product_progress_height').css({ 'height': '60%', 'background-color': '#FCB83F' })
    // } else if (total >= 40) {
    //   $('.product_progress_height').css({ 'height': '50%', 'background-color': '#FCB83F' })
    // } else if (total >= 30) {
    //   $('.product_progress_height').css({ 'height': '40%', 'background-color': '#FCB83F' })
    // } else if (total >= 20) {
    //   $('.product_progress_height').css({ 'height': '30%', 'background-color': '#FCB83F' })
    // } else if (total >= 10) {
    //   $('.product_progress_height').css({ 'height': '20%', 'background-color': '#FCB83F' })
    // } else if (total <= 10) {
    //   $('.product_progress_height').css('height', '0%')
    }
  }
  script() {
    var itemsMainDiv = ('.MultiCarousel');
    var itemsDiv = ('.MultiCarousel-inner');
    var itemWidth: any;
    $('.leftLst, .rightLst').click(function () {
      var condition = $(this).hasClass("leftLst");
      if (condition)
        click(0, this);
      else
        click(1, this)
    });
    ResCarouselSize();
    $(window).resize(function () {
      ResCarouselSize();
    });
    //this function define the size of the items
    function ResCarouselSize() {
      var incno = 0;
      var dataItems = ("data-items");
      var itemClass = ('.item');
      var id = 0;
      var btnParentSb = '';
      var itemsSplit = [];
      var sampwidth = $(itemsMainDiv).width();
      var bodyWidth = $('body').width();
      $(itemsDiv).each(function () {
        id = id + 1;
        var itemNumbers = $(this).find(itemClass).length;
        btnParentSb = $(this).parent().attr(dataItems);
        itemsSplit = btnParentSb.split(',');
        $(this).parent().attr("id", "MultiCarousel" + id);
        if (bodyWidth >= 1200) {
          incno = itemsSplit[3];
          itemWidth = sampwidth / incno;
        }
        else if (bodyWidth >= 992) {
          incno = itemsSplit[2];
          itemWidth = sampwidth / incno;
        }
        else if (bodyWidth >= 768) {
          incno = itemsSplit[1];
          itemWidth = sampwidth / incno;
        }
        else {
          incno = itemsSplit[0];
          itemWidth = sampwidth / incno;
        }
        $(this).css({ 'transform': 'translateX(0px)', 'width': itemWidth * itemNumbers });
        $(this).find(itemClass).each(function () {
          $(this).outerWidth(itemWidth);
        });
        $(".leftLst").addClass("over");
        $(".rightLst").removeClass("over");
      });
    }
    //this function used to move the items
    function ResCarousel(e, el, s) {
      var leftBtn = ('.leftLst');
      var rightBtn = ('.rightLst');
      var translateXval: any;
      var divStyle = $(el + ' ' + itemsDiv).css('transform');
      var values = divStyle.match(/-?[\d\.]+/g);
      var xds = Math.abs(parseInt(values[4]));
      if (e == 0) {
        translateXval = xds - (itemWidth * s);
        $(el + ' ' + rightBtn).removeClass("over");
        if (translateXval <= itemWidth / 2) {
          translateXval = 0;
          $(el + ' ' + leftBtn).addClass("over");
        }
      }
      else if (e == 1) {
        var itemsCondition = $(el).find(itemsDiv).width() - $(el).width();
        translateXval = xds + (itemWidth * s);
        $(el + ' ' + leftBtn).removeClass("over");
        if (translateXval >= itemsCondition - itemWidth / 2) {
          translateXval = itemsCondition;
          $(el + ' ' + rightBtn).addClass("over");
        }
      }
      $(el + ' ' + itemsDiv).css('transform', 'translateX(' + -translateXval + 'px)');
    }
    //It is used to get some elements from btn
    function click(ell, ee) {
      var Parent = "#" + $(ee).parent().attr("id");
      var slide = $(Parent).attr("data-slide");
      ResCarousel(ell, Parent, slide);
    }
    this.getSetting()
    if (localStorage.getItem('auth_token')) {
      this.getUserCart();
    }
  }
  getSetting() {
    this.service.getSettingData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.tax = res.data.setting.tax_percentage

      }
    }
    )
  }

  addFav(index, data) {
    var id;
    id = data.id
    if (localStorage.getItem('auth_token')) {
      this.service.AddRemoveFav(id).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.ngOnInit();
        } else {
          this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
      })
    }
    else {
      let favourites: any;
      favourites = JSON.parse(localStorage.getItem("fav")) || [];
      var item = favourites.findIndex(
        item => item.campaign_id === data.id
      );
      if (item > -1) {
        data.favourite = 0
        var favourite = {
          "campaign_id": data.id,
          "favourite": 0,
          "campaign_count": data.pendingItem == 1 ? 0 : data.pendingItem,
        }
        favourites.splice(item, 1)
        this.toastr.success("Removed from favouite");
      }
      if (item == -1) {
        var favourite = {
          "campaign_id": data.id,
          "favourite": 1,
          "campaign_count": data.pendingItem == 1 ? 0 : data.pendingItem,

        }
        data.favourite = 1
        favourites.push(favourite);
        this.toastr.success("Add to favouirte");
      }
      localStorage.setItem("fav", JSON.stringify(favourites));
    }
  }

  goTocartpage(data) {
    data.pendingItem=1;
    data.isCartAdded = true;
    setTimeout(() => {
      data.isCartAdded = !data.isCartAdded;
      data.cartStatus =true;
    }, 1000);
   
    if (localStorage.getItem('auth_token')) {
      var item = this.cardItem.find(
        item => item.campaign_id === data.id
      );
      if (item) {
        var quantity = item.campaign_count + data.pendingItem;
        if (quantity > data.max_per_user) {
          this.toastr.warning("You can not add more items.");
        }
        else {
          let Data = new FormData();
          Data.append('campaign_id', data.id);
          Data.append('campaign_count', quantity);
          this.service.addToCart(Data).subscribe((res: any) => {
            if (res.status == 1) {
              this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
              this.getUserCart();
            }
          })
        }
      }
      if (!item) {
        let Data = new FormData();
        Data.append('campaign_id', data.id);
        Data.append('campaign_count', data.pendingItem);
        this.service.addToCart(Data).subscribe((res: any) => {
          if (res.status == 1) {
            this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            this.getUserCart();
          }
        }
        )
      }
    }
    else {
      let product: any;
      product = JSON.parse(localStorage.getItem("campaign")) || [];
      var campaign = {
        "campaign_id": data.id,
        "campaign_count": data.pendingItem
      }
      //data.cartStatus = true;
      var item = product.find(
        item => item.campaign_id === data.id
      );
      if (item) {
        var quantity = item.campaign_count + data.pendingItem;
        if (quantity > data.max_per_user) {
          this.toastr.warning("You can not add more items.");
        } else {
          item.campaign_count = quantity;
          this.toastr.success("Deals Added successfully");
        }
      }
      if (!item) {
        product.push(campaign);
        this.toastr.success("Deals Added successfully");
      }
      var count = 0;
      for(let item of product){
        count = count + item.campaign_count 
      }
      localStorage.setItem("campaign", JSON.stringify(product));
      localStorage.setItem("campaignslength", JSON.stringify(count));
      this.comm.setgetCardValue();
    }
  }

  getHome() {
    if (localStorage.getItem('auth_token'))
      this.service.getAuthdata().subscribe((res: any) => {
        if (res.status == 1)
          this.top_banner = res.data['top_banner']
        this.all_campaigns = res.data['all_campaigns'];
        this.closing_campaigns = res.data['closing_campaigns'];
        this.products = res.data['products'];
        this.charity = res.data['charity'];
        this.winners = res.data['winners']
        this.all_campaigns.forEach(ele => {
          ele.pendingItem = 0;
          ele.isCartAdded = false;
          ele.cartStatus = false;
          this.total= 100 - ele.percentage_sold
        })
      }
      )
    else {
      this.service.getdata().subscribe((res: any) => {
        if (res.status == 1)
        this.top_banner = res.data['top_banner']
        this.all_campaigns = res.data['all_campaigns'];
        this.closing_campaigns = res.data['closing_campaigns'];
        this.products = res.data['products'];
        this.charity = res.data['charity'];
        this.winners=res.data['winners'];
        this.all_campaigns.forEach(ele => {
          var id=ele.id
          ele.pendingItem = 0;
          ele.isCartAdded = false;
          ele.cartStatus = false;
          this.total= 100 - ele.percentage_sold
        }) 
        if(localStorage.getItem('fav')){
          var favourite= JSON.parse(localStorage.getItem('fav'))||[];
          if(favourite && favourite.length>0){
            favourite.forEach(ele => {
              this.fav =ele.campaign_id
              var item = this.all_campaigns.find(
                item => item.id === this.fav
              );
              if(item){
                item.favourite=1
              }
            });
          }
        }
        if(localStorage.getItem('campaign')){
          var cartItems= JSON.parse(localStorage.getItem('campaign'))||[];
          if(cartItems && cartItems.length>0){
            var cartStatusId;
            var cartItemCount;
            cartItems.forEach(ele => {
              cartStatusId =ele.campaign_id
              cartItemCount = ele.campaign_count
              if(this.all_campaigns && this.all_campaigns.length>0){
                var item = this.all_campaigns.find(
                  item => item.id === cartStatusId
                );
                if(item){
                item.pendingItem = cartItemCount
                  item['cartStatus'] = true;
                  item['cartRemoveStatus']= false;
                }
              }
            });
          }
        }
      })
    }
  }

  getUserCart() {
    this.service.getCartData().subscribe((res: any) => {
      if (res.status == 1) {
        this.cardItem = res.data.cart
        if(this.cardItem && this.cardItem.length>0){
          var cartStatusId;
          var cartItemCount;
          this.cardItem.forEach(ele => {
            cartStatusId =ele.campaign_id
            cartItemCount = ele.campaign_count
            if(this.all_campaigns && this.all_campaigns.length>0){
              var item = this.all_campaigns.find(
                item => item.id === cartStatusId
              );
              if(item){
                item.pendingItem = cartItemCount
                item['cartStatus'] = true;
                item['cartRemoveStatus']= false;
              }
            }
          });
        }
        var count = 0;
        for(let item of this.cardItem){
          count = count + item.campaign_count 
        }
        this.comm.setAuthgetCardValue(count);
      }
    }
    )
  }

  decrement(index,data) {
    data.pendingItem--;
    if(data.pendingItem>=1){
      this.changequantity(data);
    }else{
      data.cartStatus = false;
      data.cartRemoveStatus= true;
      data.isCartAdded = true;
      setTimeout(() => {
      data.cartRemoveStatus = !data.cartRemoveStatus;
      data.isCartAdded = false;
      }, 1000);
      data.pendingItem = 0;
      this.removeItem(index,data)
    }
  }

  removeItem(index, item){
    this.cancel(index,item)
  }
  cancel(index, data) {
    if (localStorage.getItem('auth_token')) {
      this.service.removeFromCart(data.id).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.getUserCart();
        }
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      })
    }
    else {
      if (localStorage.getItem("campaign")) {
        this.cartList = JSON.parse(localStorage.getItem("campaign"));
      }
      const i = _.findIndex(this.cartList, {
        campaign_id: data.id,
      });
      if (i > -1) {
        this.cartList.splice(i, 1);
        const data: any = localStorage.setItem(
          "campaign",
          JSON.stringify(this.cartList)
        );
        var count = 0;
        for(let item of this.cartList){
          count = count + item.campaign_count 
        }
        localStorage.setItem(
          "campaignslength",
          JSON.stringify(count)
        );
        this.comm.setgetCardValue();
      }
    }
  }

  increment(data) {
    data.pendingItem++;
     this.changequantity(data);
  }

  changequantity(data) {
    if (localStorage.getItem('auth_token')) {
      var campaign = {
        "campaign_id": data.id,
        "campaign_count": data.pendingItem
      }
      let Data = new FormData();
      Data.append('campaign_id', data.id);
      Data.append('campaign_count', data.pendingItem);
      this.service.addToCart(Data).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.getUserCart();
        }
      }
      )
    } else{
      let product: any;
      product = JSON.parse(localStorage.getItem("campaign")) || []
      var item = product.find(
        item => item.campaign_id === data.id
      );
      if (item){
        item.campaign_id = item.campaign_id,
        item.campaign_count =  data.pendingItem
      }
      var count = 0;
      for(let item of product){
        count = count + item.campaign_count 
      }
      localStorage.setItem("campaign", JSON.stringify(product));
      localStorage.setItem("campaignslength", JSON.stringify(count));
      this.comm.setgetCardValue();
    }
  }

  goToproductdetail(data) {
    console.log(data)
    var id;
    id = data.id
    this.router.navigate(['productdetail', id])
  }

  onKnowMore(){
    if (localStorage.getItem('auth_token')){
      this.router.navigate(['/profile/referral']);
    }
  }

  onViewProducts(index, product){
    this.comm.viewProducts(index,product).subscribe(res=>{
  })
  }

}
