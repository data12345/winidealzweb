import { Component, OnInit } from '@angular/core';
import { ApiService } from 'src/app/services/api/api.service';
import { ToastrService } from 'ngx-toastr';
import { CommonService } from 'src/app/services/common.service';
import { Router } from '@angular/router';
import * as _ from "lodash";

@Component({
  selector: 'app-wishlistitem',
  templateUrl: './wishlistitem.component.html',
  styleUrls: ['./wishlistitem.component.scss']
})
export class WishlistitemComponent implements OnInit {
  wishlist: any[];
  tax: any;
  data:any;
  i:any;
  cardItem: any;
  cartpage:boolean
  cartList: any[];
  authStatus : boolean = false;
  constructor(private service: ApiService,private toastr: ToastrService,public comm: CommonService,private router: Router) { }

  ngOnInit() {
    this.getSetting();
    if (localStorage.getItem("campaign")) {
      this.cartList = JSON.parse(localStorage.getItem("campaign"));
    }
    if (localStorage.getItem('auth_token')){
      this.getwishlist();
      setTimeout(() => {
        this.getUserCart();
      }, 0);
      this.authStatus = true;

    }
    else{
      this.authStatus = false;
      this.getuserwishlist();
    }
 
  }
  getwishlist(){
    this.service.Getwishlist().subscribe((res: any) => {
      if (res.status == 1){
        this.wishlist=res.data
        this.wishlist.forEach(ele => {
          ele.pendingItem = 0;
          ele.isCartAdded = false;
          ele.cartStatus = false;
          ele.favourite=1
          // ele.favourite=0;
        })
        this.getUserCart();
      }
    })
  } 
addFav(index,data) {
  var id;
  id = data.campaign_id
  if (localStorage.getItem('auth_token')) {
    this.service.AddRemoveFav(id).subscribe((res: any) => {
      if (res.status == 1) {
        this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        this.ngOnInit()
      } else {
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });

      }
    }
    )
  }
  else {
    let favourites: any;
    favourites = JSON.parse(localStorage.getItem("fav")) || [];
    var item = favourites.findIndex(
      // console.log(item)
      item => item.campaign_id === data.id
    );
    console.log(item,"item")
    if (item>-1) {
      this.wishlist.splice(item, 1);

      data.favourite=0
    var favourite = {
      "campaign_id": data.id,
      "favourite": 0,
      "campaign_count":data.pendingItem == 1 ? 0: data.pendingItem,
    }
    favourites.splice(item, 1)
    console.log(favourite,"favourites====")
       this.toastr.success("Removed from favouite");
    }
    if (item==-1) {
      var favourite = {
      "campaign_id": data.id,
      "favourite": 1,
      "campaign_count":data.pendingItem == 1 ? 0 : data.pendingItem,

    }
    data.favourite=1
    favourites.push(favourite);
      console.log(favourite,"favourites====")
      this.toastr.success("Add to favouirte");
    }
    console.log(favourite,"favourites====")
    localStorage.setItem("fav", JSON.stringify(favourites));
}  
}
getuserwishlist(){
  this.service.getGuestUserwishlist({ campaign: JSON.parse(localStorage.getItem("fav")) }).subscribe((res: any) => {
    console.log(res);
    if (res.status == 1) {
      this.wishlist=res.data
      this.wishlist.forEach(ele => {
        ele.favourite=1
        ele.pendingItem = 0;
        ele.isCartAdded = false;
        ele.cartStatus = false;
        // ele.favourite=0;
      })
     }

        if(localStorage.getItem('campaign')){
        var cartItems= JSON.parse(localStorage.getItem('campaign'))||[];
        if(cartItems && cartItems.length>0){
          var cartStatusId;
          var cartItemCount;
          cartItems.forEach(ele => {
            cartStatusId =ele.campaign_id
            cartItemCount = ele.campaign_count
            if(this.wishlist && this.wishlist.length>0){
              var item = this.wishlist.find(
                item => item.id === cartStatusId
              );
              if(item){
              item.pendingItem = cartItemCount
                item['cartStatus'] = true;
                item['cartRemoveStatus']= false;
              }
            }
          });
        }
      }
    }
  )
}
gotoHome(){
  this.router.navigate(['/homepage'])
}
gocartpage(){
  this.router.navigate(['/otp'])
}
goTocartpage(data) {
  data.pendingItem=1;
  data.isCartAdded = true;
  setTimeout(() => {
    data.isCartAdded = !data.isCartAdded;
    data.cartStatus =true;
  }, 1000);
  if (localStorage.getItem('auth_token')) {
    var item = this.cardItem.find(
      item => item.campaign_id === data.campaign_id

    );
    if (item) {
      var quantity = item.campaign_count + data.pendingItem;
      if (quantity > data.max_per_user) {
        this.toastr.warning("You can not add more items.");
      }
      else {
        let Data = new FormData();
        Data.append('campaign_id', data.campaign_id);
        Data.append('campaign_count', quantity);
        this.service.addToCart(Data).subscribe((res: any) => {
          if (res.status == 1) {
            this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            this.getUserCart();
          }
        }
        )
      }


    }
    if (!item) {
      let Data = new FormData();
      Data.append('campaign_id', data.campaign_id);
      Data.append('campaign_count', data.pendingItem);
      this.service.addToCart(Data).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.getUserCart();
        }
      }
      )
    }

  }
  else {
    let product: any;
    product = JSON.parse(localStorage.getItem("campaign")) || [];
    var campaign = {
      "campaign_id": data.id,
      "campaign_count": data.pendingItem
    }
    var item = product.find(
      item => item.campaign_id === data.id
    );
    if (item) {
      var quantity = item.campaign_count + data.pendingItem;
      if (quantity > data.max_per_user) {
        this.toastr.warning("You can not add more items.");
      } else {
        item.campaign_count = quantity;
        this.toastr.success("Deals Added successfully");
      }
    }
    if (!item) {
      product.push(campaign);
      this.toastr.success("Deals Added successfully");
    }
    var count = 0;
    for(let item of product){
      count = count + item.campaign_count 
    }
    localStorage.setItem("campaign", JSON.stringify(product));
    localStorage.setItem("campaignslength", JSON.stringify(count));
    this.comm.setgetCardValue();
  }
}

  getSetting() {
    this.service.getSettingData().subscribe((res: any) => {
      if (res.status == 1) {
        this.tax=res.data.setting.tax_percentage
      }
    })
  }

  getUserCart() {
    this.service.getCartData().subscribe((res: any) => {
      if (res.status == 1) {
        this.cardItem = res.data.cart
        if(this.cardItem && this.cardItem.length>0){
          var cartStatusId;
          var cartItemCount;
          this.cardItem.forEach(ele => {
            cartStatusId =ele.campaign_id
            cartItemCount = ele.campaign_count
            if(this.wishlist && this.wishlist.length>0){
              var item = this.wishlist.find(
                item => item.campaign_id === cartStatusId
              );
              console.log(item,"itemmmmm");
              if(item){
                item.pendingItem = cartItemCount
                item['cartStatus'] = true;
                item['cartRemoveStatus']= false;
              }
            }
          });
        }
        var count = 0;
        for(let item of this.cardItem){
          count = count + item.campaign_count 
        }
        this.comm.setAuthgetCardValue(count);
      }
    }
    )
  }


  decrement(index,data) {
    data.pendingItem--;
    if(data.pendingItem>=1){
      this.changequantity(data);
    }else{
      data.cartStatus = false;
      data.cartRemoveStatus= true;
      data.isCartAdded = true;
      setTimeout(() => {
      data.cartRemoveStatus = !data.cartRemoveStatus;
      data.isCartAdded = false;
      }, 1000);
      data.pendingItem = 0;
      this.removeItem(index,data)
    }
  }

  removeItem(index, item){
    this.cancel(index,item)
  }

  cancel(index, data) {
    if (localStorage.getItem('auth_token')) {
    console.log(data,"auth");
      this.service.removeFromCart(data.campaign_id).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.getUserCart();
        }
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      })
    }
    else {
      if (localStorage.getItem("campaign")) {
        this.cartList = JSON.parse(localStorage.getItem("campaign"));
      }
      const i = _.findIndex(this.cartList, {
        campaign_id: data.id,
      });
      if (i > -1) {
        this.cartList.splice(i, 1);
        const data: any = localStorage.setItem(
          "campaign",
          JSON.stringify(this.cartList)
        );
        var count = 0;
        for(let item of this.cartList){
          count = count + item.campaign_count 
        }
        localStorage.setItem(
          "campaignslength",
          JSON.stringify(count)
        );
        this.comm.setgetCardValue();
      }
    }
  }

  increment(data) {
    data.pendingItem++;
     this.changequantity(data);
  }

  changequantity(data) {
    if (localStorage.getItem('auth_token')) {
      var campaign = {
        "campaign_id": data.campaign_id,
        "campaign_count": data.pendingItem
      }
      let Data = new FormData();
      Data.append('campaign_id', data.campaign_id);
      Data.append('campaign_count', data.pendingItem);
      this.service.addToCart(Data).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.getUserCart();
        }else{
          this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
      }
      )
    } else{
      let product: any;
      product = JSON.parse(localStorage.getItem("campaign")) || []
      var item = product.find(
        item => item.campaign_id === data.id
      );
      if (item){
        item.campaign_id = item.campaign_id,
        item.campaign_count =  data.pendingItem
      }
      var count = 0;
      for(let item of product){
        count = count + item.campaign_count 
      }
      localStorage.setItem("campaign", JSON.stringify(product));
      localStorage.setItem("campaignslength", JSON.stringify(count));
      this.comm.setgetCardValue();
    }
  }

}

