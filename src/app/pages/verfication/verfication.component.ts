import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-verfication',
  templateUrl: './verfication.component.html',
  styleUrls: ['./verfication.component.scss']
})
export class VerficationComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
  }
  goTologin() {
    this.router.navigate(['/login'])
  }
}
