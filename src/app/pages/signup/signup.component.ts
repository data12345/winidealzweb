import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgForm } from '@angular/forms';
import { ApiService } from '../../services/api/api.service'
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-signup',
  templateUrl: './signup.component.html',
  styleUrls: ['./signup.component.scss']
})
export class SignupComponent implements OnInit {

  constructor(private router: Router,public service:ApiService, private toastr:ToastrService) { }
  confirmError: boolean;
  countryStatus: boolean = true;
  countryCode:any=[];
  first_name:any;
  last_name:any;
  number:any;
  Email:any;
  Password:any;
  referral_code:any
  config:object;
    url: string;
    file;
    nationality: any= [];
    country_code:any='+91';
  gender: any = ['Male', 'Female', 'Other'];
  selectedNationality: string;

  ngOnInit() {
    // this.url =this.service.baseUrl
    this.service.geNationality().subscribe((res:any) => {
      this.nationality = res;
      this.selectedNationality = "United Arab Emirates";
    })
    
    this.service.getCountryCode().subscribe((res:any) => {
      this.countryCode = res.countryArray
      console.log(this.countryCode.length,res.countryArray.length,)
        this.config = {
          displayKey:"Code", //if objects array passed which key to be displayed defaults to description
          search: true, //true/false for the search functionlity defaults to false,
          height: "150px", //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
          placeholder: "Code", // text to be displayed when no item is selected defaults to Select,
          customComparator: () => { }, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
          limitTo: this.countryCode.length, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
          moreText: "more", // text to be displayed whenmore than one items are selected like Option 1 + 5 more
          noResultsFound: "No results found!", // text to be displayed when no items are found while searching
          searchPlaceholder: "Search", // label thats displayed in search input,
          searchOnKey: "Code" // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
        };
        });
  }
  onSubmit(form :NgForm){
    console.log(form.value)
    if (this.country_code == '' || this.country_code== undefined)
    return this.toastr.error('Select Country Code')
    if(form.value.referral_code == undefined) {
      form.value.referral_code = "";
    }
    console.log(form.value.refferal_code)
    let Data = new FormData();
    for (let key in form.value) {
      Data.append(`${key}`, form.value[key]);
      console.log(Data,`${key}`)
    }
    Data.append('country_code',this.country_code);
    Data.append('image',this.file);
   
    if (form.valid) {
      this.service.signUp(Data).subscribe((res: any) => {
        if (res.status== 1) {
            this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            localStorage.setItem("sigUp", JSON.stringify(res.data));
           this.router.navigate(['/otp'])
          } else {
            this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          }
        }
        );
      }
  }
  
  getguestUserFav() {
    let body ={}
    this.service.AddRemoveFav(body).subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
      
      }
    }
    )
  }
  goToverfication() {
    this.router.navigate(['verfication'])
  };
  goTologin() {
    this.router.navigate(['login'])
  }
  goTootp() {
    this.router.navigate(['/otp'])
  }
  checkConfirm(pass, confirmpass) {
    String(confirmpass.value) == String(pass.value);
    this.confirmError = !(String(confirmpass.value) == String(pass.value))
    return String(confirmpass.value) == String(pass.value)
  }
  selectionChanged(event) {
    console.log(event.value)
    if (event.value !=undefined) {
     this.countryStatus = true;
     this.country_code = event.value.Code
    }
    else
    {
      this.countryStatus = false;
      this.country_code='';
      // this.user.country_code=''

    }
  }
  uploadProfileImage(event) {
    this.file = event.target.files[0];
    console.log ("evec",event)
    if (event.target.files[0].type.indexOf("image/") == 0) {
      var reader = new FileReader();
      reader.readAsDataURL(event.target.files[0]); // read file as data url
      reader.onload = (event: any) => { // called once readAsDataURL is completed
        this.url = event.target.result;
      
    }
  }
    else {
      this.toastr.warning('Invalid Image', "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });

    }

  }
}
