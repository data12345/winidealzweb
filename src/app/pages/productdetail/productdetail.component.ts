import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { ApiService } from '../../services/api/api.service'
import {  CommonService} from '../../services/common.service'

import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";

@Component({
  selector: 'app-productdetail',
  templateUrl: './productdetail.component.html',
  styleUrls: ['./productdetail.component.scss']
})
export class ProductdetailComponent implements OnInit {
  id: number;
  campionsDetails: any
  tax: any;
  cardItem: any;
  fav: any;
  total: number;
  cartList: any[];
  pendingItem : any = 0;
  isCartAdded = false;
  cartStatus = false;
  cartRemoveStatus =false;
  constructor(private router: Router, public service: ApiService, private route: ActivatedRoute,public comm: CommonService, 
    private toastr: ToastrService) { }

  ngOnInit() {
   
    if (this.total > 0 || this.total >= 30) {
      $('.product_progress_height').css({ 'height': '40%', 'background-color': '#0db14b	' })
    } else if (this.total > 30 || this.total <= 40) {
      $('.product_progress_height').css({ 'height': '30%', 'background-color': '#fcb83f' })
    } else {
      $('.product_progress_height').css({ 'height': '80%', 'background-color': '#f44336' })
    // if (total >= 90) {
    //   $('.product_progress_height').css({ 'height': '100%', 'background-color': '#1ea842' })
    // } else if (total >= 80) {
    //   $('.product_progress_height').css({ 'height': '90%', 'background-color': '#1ea842' })
    // } else if (total >= 70) {
    //   $('.product_progress_height').css({ 'height': '80%', 'background-color': '#1ea842' })
    // } else if (total >= 60) {
    //   $('.product_progress_height').css({ 'height': '70%', 'background-color': '#FCB83F' })
    // } else if (total >= 50) {
    //   $('.product_progress_height').css({ 'height': '60%', 'background-color': '#FCB83F' })
    // } else if (total >= 40) {
    //   $('.product_progress_height').css({ 'height': '50%', 'background-color': '#FCB83F' })
    // } else if (total >= 30) {
    //   $('.product_progress_height').css({ 'height': '40%', 'background-color': '#FCB83F' })
    // } else if (total >= 20) {
    //   $('.product_progress_height').css({ 'height': '30%', 'background-color': '#FCB83F' })
    // } else if (total >= 10) {
    //   $('.product_progress_height').css({ 'height': '20%', 'background-color': '#FCB83F' })
    // } else if (total <= 10) {
    //   $('.product_progress_height').css('height', '0%')
    }
    window.scroll(0, 0);
    this.route.params.subscribe(params => {
      this.id = params['id']
      this.getProductDetails();
      this.getSetting();
      this.getUserCart()
    });

    if (localStorage.getItem("campaign")) {
      this.cartList = JSON.parse(localStorage.getItem("campaign"));
      for(let list of  this.cartList){
        if(list.campaign_id == (this.id)){
          this.pendingItem = list.campaign_count
          this.cartStatus = true;
        }
      }
    }
  }
  goTocartpage(data) {
    this.pendingItem=1;
    this.isCartAdded = true;
    setTimeout(() => {
      this.isCartAdded = !this.isCartAdded;
      this.cartStatus =true;
    }, 1000);
   
    if (localStorage.getItem('auth_token')) {
      let Data = new FormData();
      Data.append('campaign_id', data.id);
      Data.append('campaign_count', data.pendingItem);
      this.service.addToCart(Data).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
      }
      )
      var item = this.cardItem.find(
        item => item.campaign_id === data.id

      );
      if (item) {
        console.log( item.campaign_count ,data.max_per_user, data.pendingItem,"dstssss")
        var quantity = item.campaign_count + this.pendingItem;
        if (quantity > data.max_per_user) {
          this.toastr.warning("You can not add more items.");
        }
        else{
          let Data = new FormData();
        Data.append('campaign_id', data.id);
        Data.append('campaign_count', quantity);
        this.service.addToCart(Data).subscribe((res: any) => {
          if (res.status == 1) {
            this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            this.getUserCart();
          }
        }
        )
        }


      }
      if (!item) {
        let Data = new FormData();
        Data.append('campaign_id', data.id);
        Data.append('campaign_count',this.pendingItem);
        this.service.addToCart(Data).subscribe((res: any) => {
          if (res.status == 1) {
            this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            this.getUserCart();
          }
        }
        )
      }

    }
    else {
   let product: any;
      product = JSON.parse(localStorage.getItem("campaign")) || [];
      var campaign = {
        "campaign_id": data.id,
        "campaign_count": this.pendingItem
      }
      var item = product.find(
        item => item.campaign_id === data.id
      );
      if (item) {
        var quantity = item.campaign_count + this.pendingItem;
        if (quantity > data.max_per_user) {
          this.toastr.warning( 'You can not add more items.',"", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        } else {
          item.campaign_count = quantity;
          this.toastr.success( 'Deals Added successfully',"", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
      }
      if (!item) {
        product.push(campaign);
        this.toastr.success( 'Deals Added successfully',"", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      }
      localStorage.setItem("campaign", JSON.stringify(product));
      localStorage.setItem("campaignslength", JSON.stringify(product.length));
      this.comm.setgetCardValue();
    }
  }
 
  getUserCart() {
    this.service.getCartData().subscribe((res: any) => {
      if (res.status == 1) {
        this.cardItem = res.data.cart
        if(this.cardItem && this.cardItem.length>0){
          var cartStatusId;
          var cartItemCount;
          this.cardItem.forEach(ele => {
            cartStatusId =ele.campaign_id
            cartItemCount = ele.campaign_count
            if(ele.campaign_id == (this.id)){
              this.pendingItem = cartItemCount
              this.cartStatus = true;
              this.cartRemoveStatus = false;
            }
          });
        }
        var count = 0;
        for(let item of this.cardItem){
          count = count + item.campaign_count 
        }
        this.comm.setAuthgetCardValue(count);
      }
    })
  }
  
  getProductDetails() {
    this.service.getProductData(this.id).subscribe((res: any) => {
      if (res.status == 1) {
        this.campionsDetails = res.data
        var total_pro =this.campionsDetails. inventory;
        var sell_pro = this.campionsDetails.inventory_sold;
        this. total = sell_pro / total_pro * 100
        if(localStorage.getItem("fav"))
        var favourite= JSON.parse(localStorage.getItem('fav'))||[]
        if(favourite && favourite.length>0){
          favourite.forEach(ele => {
            this.fav =ele.campaign_id
          });
            if(this.campionsDetails.id === this.fav)
              this.campionsDetails.favourite=1
        }
      }
    }
    )
  }
  getSetting() {
    this.service.getSettingData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.tax = res.data.setting.tax_percentage

      }
    }
    )
  }
  addFav(index,data) {
    var id;
    id = data.id
    if (localStorage.getItem('auth_token')) {
      this.service.AddRemoveFav(id).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.ngOnInit();
        } else {
          this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });

        }
      }
      )
    }
    else {
      let favourites: any;
      favourites = JSON.parse(localStorage.getItem("fav")) || [];
      var item = favourites.findIndex(
        // console.log(item)
        item => item.campaign_id === data.id
      );
      console.log(item,"item")
      if (item>-1) {
        data.favourite=0
      var favourite = {
        "campaign_id": data.id,
        "favourite": 0,
        "campaign_count":this.pendingItem == 1 ? 0: this.pendingItem,
      }
      favourites.splice(item, 1)
      console.log(favourite,"favourites====")
         this.toastr.success("Removed from favouite");
      }
      if (item==-1) {
        var favourite = {
        "campaign_id": data.id,
        "favourite": 1,
        "campaign_count":this.pendingItem == 1 ? 0 : this.pendingItem,

      }
      data.favourite=1
      favourites.push(favourite);
        this.toastr.success("Add to favouirte");
      }
      localStorage.setItem("fav", JSON.stringify(favourites));
}  
  }
  
  decrement() {
    this.pendingItem--;
    if(this.pendingItem>=1){
      this.changequantity();
    }else{
      this.cartStatus = false;
      this.cartRemoveStatus= true;
      this.isCartAdded = true;
      setTimeout(() => {
        this.cartRemoveStatus = !this.cartRemoveStatus;
        this.isCartAdded = false;
      }, 1000);
      this.pendingItem = 0;
      this.removeItem()
    }
  }

  increment() {
    this.pendingItem++;
    this.changequantity();
  }

  removeItem(){
    this.cancel()
  }
  cancel() {
    if (localStorage.getItem('auth_token')) {
      this.service.removeFromCart(this.id).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.getUserCart();
        }
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      })
    }
    else {
      if (localStorage.getItem("campaign")) {
        this.cartList = JSON.parse(localStorage.getItem("campaign"));
      }
      const i = _.findIndex(this.cartList, {
        campaign_id: Number(this.id)
      });
      if (i > -1) {
        this.cartList.splice(i, 1);
        const data: any = localStorage.setItem(
          "campaign",
          JSON.stringify(this.cartList)
        );
        var count = 0;
        for(let item of this.cartList){
          count = count + item.campaign_count 
        }
        localStorage.setItem(
          "campaignslength",
          JSON.stringify(count)
        );
        this.comm.setgetCardValue();
      }
    }
  }

  changequantity() {
    if (localStorage.getItem('auth_token')) {
      var campaign = {
        "campaign_id": this.id,
        "campaign_count": this.pendingItem
      }
      let Data = new FormData();
      Data.append('campaign_id', String(this.id));
      Data.append('campaign_count', this.pendingItem);
      this.service.addToCart(Data).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.getUserCart();
        }else{
          this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
      })
    } else {
      let product: any;
      product = JSON.parse(localStorage.getItem("campaign")) || []
      var item = product.find(
        item => item.campaign_id == this.id
      );
      if (item){
        item.campaign_id = item.campaign_id,
        item.campaign_count =  this.pendingItem
      }
      var count = 0;
      for(let item of product){
        count = count + item.campaign_count 
      }
      localStorage.setItem("campaign", JSON.stringify(product));
      localStorage.setItem("campaignslength", JSON.stringify(count));
      this.comm.setgetCardValue();
    }
  }

}