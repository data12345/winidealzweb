import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { MaterialModule } from './material/material.module';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { NgOtpInputModule } from  'ng-otp-input';
import { NavbarComponent } from './compoment/navbar/navbar.component';
import { HomepageComponent } from './pages/homepage/homepage.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AllcampaignsComponent } from './pages/allcampaigns/allcampaigns.component';
import { ProductdetailComponent } from './pages/productdetail/productdetail.component';
import { CartpageComponent } from './pages/cartpage/cartpage.component';
import { LoginComponent } from './pages/login/login.component';
import { SignupComponent } from './pages/signup/signup.component';
import { VerficationComponent } from './pages/verfication/verfication.component';
import { ForgetpasswordComponent } from './pages/forgetpassword/forgetpassword.component';
import { PaymentComponent } from './pages/payment/payment.component';
import { PaynowComponent } from './pages/paynow/paynow.component';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import { ProfileComponent } from './pages/profile/profile.component';
import { OrderdetailComponent } from './pages/orderdetail/orderdetail.component';
import { FooterComponent } from './pages/footer/footer.component';
import { OtpComponent } from './otp/otp.component';
import { ProductdetailticketComponent } from './productdetailticket/productdetailticket.component';
import { ContactusComponent } from './contactus/contactus.component';
import { FaqComponent } from './faq/faq.component';
import { AboutusComponent } from './aboutus/aboutus.component';
import { TermsconditionComponent } from './termscondition/termscondition.component';
import { PrivacypolicyComponent } from './privacypolicy/privacypolicy.component';
import { HowweworkComponent } from './howwework/howwework.component';
import { WhoweareComponent } from './whoweare/whoweare.component';
import { FormsModule,ReactiveFormsModule } from '@angular/forms'
import { OtpforgotComponent } from './otpforgot/otpforgot.component';
import { SelectDropDownModule } from "ngx-select-dropdown";
import { ToastrModule } from 'ngx-toastr';
import { ResetpasswordComponent } from './resetpassword/resetpassword.component';
import {GetInterceptorService} from './services/interceptors/get-interceptor.service';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import {NumberOnlyDirective  } from '.././app/directives/number-only/number-only.directive';
import {CharacterOnlyDirective  } from '../app/directives/character-only/character-only.directive';
import {DecimalnumberDirective  } from '../app/directives/decimal-only/decimalnumber.directive';
import { WishlistitemComponent } from './pages/wishlistitem/wishlistitem.component';
import {NgxPaginationModule} from 'ngx-pagination'; // <-- import the module

import { OwlModule } from 'ngx-owl-carousel';
import { ProductDialogComponent } from './modal/product-dialog/product-dialog.component';
import { AddressDialogComponent } from './modal/address-dialog/address-dialog.component';

@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    HomepageComponent,
    AllcampaignsComponent,
    ProductdetailComponent,
    CartpageComponent,
    LoginComponent,
    SignupComponent,
    VerficationComponent,
    ForgetpasswordComponent,
    PaymentComponent,
    PaynowComponent,
    WishlistitemComponent,
    ProfileComponent,
    OrderdetailComponent,
    FooterComponent,
    OtpComponent,
    ProductdetailticketComponent,
    ContactusComponent,
    FaqComponent,
    AboutusComponent,
    TermsconditionComponent,
    PrivacypolicyComponent,
    HowweworkComponent,
    WhoweareComponent,
    OtpforgotComponent,
    ResetpasswordComponent,
    NumberOnlyDirective,
    DecimalnumberDirective,
    CharacterOnlyDirective,
    ProductDialogComponent,
    AddressDialogComponent

  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    MaterialModule,
    BrowserAnimationsModule,
    NgOtpInputModule,
    FormsModule,
    ReactiveFormsModule,
    HttpClientModule,
    SelectDropDownModule,
    SlickCarouselModule,
    OwlModule,
    NgxPaginationModule,
    ToastrModule.forRoot({
      // timeOut: 10000,
      positionClass: 'toast-top-right',
      preventDuplicates: true,
    })
  ],
  entryComponents:[
    ProductDialogComponent,
    AddressDialogComponent
  ],
  exports: [
    NumberOnlyDirective,
],
  providers: [
    { provide: HTTP_INTERCEPTORS,
      useClass: GetInterceptorService,
      multi: true},
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
