import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api/api.service';
import { ToastrService } from 'ngx-toastr';
import { NgForm } from '@angular/forms';
@Component({
  selector: 'app-otp',
  templateUrl: './otp.component.html',
  styleUrls: ['./otp.component.scss']
})
export class OtpComponent implements OnInit {
  phone: any;
  code: any = ''
  timeLeft: number = 60;
  interval;
  role_id: any;
  id: any;
  constructor(private router: Router, private service: ApiService, private toastr: ToastrService) { }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem("sigUp"))) {
      this.role_id = JSON.parse(localStorage.getItem("sigUp")).role_id;
      this.id = JSON.parse(localStorage.getItem("sigUp")).id;
      this.phone = JSON.parse(localStorage.getItem("sigUp")).number;
    }
    if(JSON.parse(localStorage.getItem('forgotPassword')))
    {
    this.id=JSON.parse(localStorage.getItem('forgotPassword')).id
    this.role_id=JSON.parse(localStorage.getItem('forgotPassword')).role_id
    }
    if(JSON.parse(localStorage.getItem('updateProfile')))
    {
    this.id=JSON.parse(localStorage.getItem('updateProfile')).id
    this.role_id=JSON.parse(localStorage.getItem('updateProfile')).role_id
    }
    
    this.startTimer(60)
  }
  startTimer(seconds) {
    this.timeLeft = seconds
    this.interval = setInterval(() => {
      if (this.timeLeft > 0) {
        this.timeLeft--;
        if (this.timeLeft <= 0) {
          clearInterval(this.interval);
        }
      }
    }, 1000)
  }
  reset() {
    console.log(this.id)
    this.startTimer(60)
    let Data = new FormData();
    Data.append('id', this.id);
    this.service.resetOpt(Data).subscribe((res: any) => {
      console.log(res);
      if (res.status == 1)
        this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        this.startTimer(60)
      })
      
  }
  onOtpChange(event) {
    console.log(event)
    this.code = event
}

  onSubmit(form: NgForm) {
   console.log(form,this.role_id,this.code,this.id);
    let Data = new FormData();
    Data.append('id', this.id);
    Data.append('role_id', this.role_id);
    Data.append('otp', this.code);
    if(this.code =='')
    return this.toastr.error('Please enter Otp')
    if (this.role_id && this.id && this.code) {
      if(localStorage.getItem('updateProfile')){
        var profileUpdate = JSON.parse(localStorage.getItem('updateProfile'));
        let profileData = new FormData();
        profileData.append('id', profileUpdate.id);
        profileData.append('role_id', profileUpdate.role_id);
        profileData.append('otp', this.code);
        profileData.append('country_code', profileUpdate.country_code);
        profileData.append('number', profileUpdate.number);
        this.service.editProfileVerify(profileData).subscribe((res: any) => {
          if (res.status == 1) {
            if (localStorage.getItem('campaign'))
            console.log(localStorage.getItem('campaign'))
            let data={'campaign': JSON.parse(localStorage.getItem('campaign'))
            }
              this.service.AddGuestUserCard(data).subscribe((res: any) => {
                if (res.status== 1) {
                  console.log(res)
                }
              })
          if (localStorage.getItem('sigUp')) {
            this.toastr.success('Updated successfully', "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            localStorage.setItem('sigUp',JSON.stringify(res.data));
            localStorage.setItem('auth_token',res.data.auth_token);
            this.getfavList();
            this.getCartlist()
            this.router.navigate(['/homepage'])
            localStorage.removeItem("updateProfile");
          }
          else{
            this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          }}
        })
      }else{
        this.service.verifyOtp(Data).subscribe((res: any) => {
          console.log(res);
          if (res.status == 1) {
            if (localStorage.getItem('campaign'))
            console.log(localStorage.getItem('campaign'))
            let data={'campaign': JSON.parse(localStorage.getItem('campaign'))
            }
              this.service.AddGuestUserCard(data).subscribe((res: any) => {
                if (res.status== 1) {
                  console.log(res)
                }
              })
            if (localStorage.getItem('sigUp')) {
            this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            localStorage.setItem('sigUp',JSON.stringify(res.data));
            localStorage.setItem('auth_token',res.data.auth_token);
            this.getfavList();
            this.getCartlist()
            this.router.navigate(['/'])
          }
            else{
              this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            }
            if (localStorage.getItem('forgot')) {
              console.log('data')
              // this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
              this.router.navigate(['/resetpassword']);
            }
            else{
              this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
            }
            if (localStorage.getItem('updateProfile')) {
              console.log('data')
              this.toastr.success('Updated successfully', "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
              this.router.navigate(['/homepage']);
              localStorage.setItem('sigUp',JSON.stringify(res.data));
              localStorage.setItem('auth_token',res.data.auth_token);
              localStorage.removeItem("updateProfile");
            }
          }
            else {
            this.toastr.error(res.uiMessage);
          }

        }
        )
      }
    }
  }

  goTosignup() {
    this.router.navigate(['/signup'])
  };
  goToforgetpassword() {
    this.router.navigate(['/forgetpassword'])
  };
  goTohomepage() {
    this.router.navigate(['/homepage'])
  }
  getCartlist(){
    if (localStorage.getItem('campaign'))
          console.log(localStorage.getItem('campaign'))
          let data={'campaign':
          JSON.parse(localStorage.getItem('campaign'))
          }
            this.service.AddGuestUserCard(data).subscribe((res: any) => {
              if (res.status== 1) {
                localStorage.removeItem("campaignslength");
                localStorage.removeItem("campaign");
                console.log(res)
              }
            })
  }
getfavList(){
  if (localStorage.getItem('fav')) {
    let data1 = {
      'campaign': JSON.parse(localStorage.getItem('fav'))
    }
    this.service.AddFavguestUser(data1).subscribe((res: any) => {
      if (res.status == 1) {
        console.log(res)
        localStorage.removeItem("fav");

      }
      else {
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      }
    })

  }
}
}