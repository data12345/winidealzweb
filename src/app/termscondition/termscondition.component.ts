import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api/api.service';

@Component({
  selector: 'app-termscondition',
  templateUrl: './termscondition.component.html',
  styleUrls: ['./termscondition.component.scss']
})
export class TermsconditionComponent implements OnInit {
  terms: any;

  constructor(private service: ApiService) { }

  ngOnInit() {
    window.scroll(0, 0);
    this.getContactusdata()
  }
  getContactusdata() {
    this.service.getStaticData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
      this.terms=res.data.terms.data
      }
    }
    )
  }
}