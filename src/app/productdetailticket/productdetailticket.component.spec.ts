import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductdetailticketComponent } from './productdetailticket.component';

describe('ProductdetailticketComponent', () => {
  let component: ProductdetailticketComponent;
  let fixture: ComponentFixture<ProductdetailticketComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductdetailticketComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductdetailticketComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
