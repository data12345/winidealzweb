import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators, FormControl, NgForm } from "@angular/forms";
import { ToastrService } from 'ngx-toastr';
import { ApiService } from 'src/app/services/api/api.service';

@Component({
  selector: 'app-resetpassword',
  templateUrl: './resetpassword.component.html',
  styleUrls: ['./resetpassword.component.scss']
})
export class ResetpasswordComponent implements OnInit {

  confirmError: boolean
  public newPassword: any
  public oldPassword: any
  id: any;
  role_id: any;
  cpassword: any;

  constructor(private toastr: ToastrService, private router: Router,public service: ApiService) { }

  ngOnInit() {
    if (JSON.parse(localStorage.getItem('forgotPassword'))) {
      this.id = JSON.parse(localStorage.getItem('forgotPassword')).id
      this.role_id = JSON.parse(localStorage.getItem('forgotPassword')).role_id
    }

  }

  onPasswordSubmit(f: NgForm) {
    console.log(f)
    if (this.newPassword != this.cpassword) {
      this.confirmError = true
      console.log(this.confirmError, this.newPassword, this.cpassword)
    }
    else {
      this.confirmError = false;
      let Data = new FormData();
        Data.append(`id`, this.id)
        Data.append(`role_id`,this.role_id)
        Data.append(`new_password`, f.value.newPassword)
        Data.append(`confirm_password`, f.value.cpassword)
      this.service.resetPaasword(Data).subscribe((res: any) => {
        if (res.status == 1) {
          console.log(res)
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
          this.router.navigate(['/login'])
          localStorage.removeItem('forgot');
          localStorage.removeItem('forgotPassword');
         }
        else {
          this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
      });
    }

  }
}