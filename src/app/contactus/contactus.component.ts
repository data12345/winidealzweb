import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api/api.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-contactus',
  templateUrl: './contactus.component.html',
  styleUrls: ['./contactus.component.scss']
})
export class ContactusComponent implements OnInit {
  message: any;
  Email: any;
  country: any;
  address: any;
  number: any;
  status:boolean=false;

  constructor(private router: Router, public service: ApiService, private toastr: ToastrService) { }

  ngOnInit() {
    // window.scroll(0, 0);  
  }
  onSubmit(form) {
    this.status=true
    let Data = new FormData();

    for (let key in form.value) {
      Data.append(`${key}`, form.value[key]);
      console.log(Data, `${key}`)
    }
   
    if (form.valid) {
      this.service.Contactus(Data).subscribe((res: any) => {
        if (res.status == 1) {
          this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
           
          form.reset(); 
          this.status=false
          // form.setErrors(null);
          // form.markAsPristine();
          //  form.markAllAsTouched();
        } else {
          this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        }
      }
      );
    }
  }
}