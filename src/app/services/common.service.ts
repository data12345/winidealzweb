import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";
import { BehaviorSubject, Observable } from 'rxjs';
import { Router } from '@angular/router';
import { MatDialogRef, MatDialog } from '@angular/material';
import { ProductDialogComponent } from '../modal/product-dialog/product-dialog.component';
import { AddressDialogComponent } from '../modal/address-dialog/address-dialog.component';

@Injectable({
  providedIn: "root"
})
export class CommonService {
  cardData = new BehaviorSubject<any>(0);  
  AuthcardData = new BehaviorSubject<any>(0);
  profileData = new BehaviorSubject<any>('');

  // public BASE_URL = "http://192.168.1.119:4003/api/user/";
  // public IMAGE_URL = "http://192.168.1.119:4003";
  public BASE_URL = "https://appgrowthcompany.com:4003/api/user/";
  public IMAGE_URL = "https://appgrowthcompany.com:4003";
  public userProfile: any;
  public signUpProfile: any;
  public signUpAuthorization: any;
  public serviceId;
  public countryCode;
  public phone;
  public authToken;
  public cartLength: number;
  campaignslength: any;
  Authcampaignslength: any;

  constructor(private http: HttpClient, public router: Router,  private dialog : MatDialog) {
    if (localStorage.getItem('campaignslength')) {
      this.setgetCardValue();
      const signUp = localStorage.getItem("sigUp");
      this.profileData = new BehaviorSubject<any>(signUp);
    }
  }

  getCardValue(): Observable<boolean> {
    return this.cardData.asObservable();
  }
  getAuthCardValue(): Observable<boolean> {
    return this.AuthcardData.asObservable();
  }
  setAuthgetCardValue(length) {
    console.log(length,"length")
    this.AuthcardData.next(length);
  }
  setgetCardValue() {
    this.campaignslength = JSON.parse(localStorage.getItem('campaignslength'));
    this.cardData.next(this.campaignslength);
  }

  setProfile(value: any){
    this.profileData.next(value);
  }
  
  getProfile(): Observable<boolean> {
    return this.profileData.asObservable()
  }

  viewProducts(selectedIndex, products){
		let dialogRef : MatDialogRef<ProductDialogComponent>;
		dialogRef = this.dialog.open(ProductDialogComponent);
    dialogRef.componentInstance.selectedIndex = selectedIndex;
    dialogRef.componentInstance.product = products;
		return dialogRef.afterClosed();
	}

  addAddress(){
		let dialogRef : MatDialogRef<AddressDialogComponent>;
		dialogRef = this.dialog.open(AddressDialogComponent);
		return dialogRef.afterClosed();
	}
} 
