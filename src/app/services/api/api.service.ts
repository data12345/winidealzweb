import { Injectable } from '@angular/core';
import { HttpClient, HttpEventType, HttpHeaders } from '@angular/common/http';
// import {UrlService} from '../url/url.service';
import { map } from 'rxjs/operators';


@Injectable({
  providedIn: 'root'
})
export class ApiService {
  countryCode: any;
  baseUrl: string = "https://webdevelopmentsolution.net/winidealz_new/api/";

  constructor(private http: HttpClient, ) {
  }
  getCountryCode() {
    return this.http
      .get<any>("assets/json/json/countryCode.json")
      .pipe(map(response => response));
  }

  geNationality() {
    return this.http
      .get<any>("assets/json/json/nationality.json")
      .pipe(map(response => response));
  }
  signUp(body) {
    return this.http.post(`${this.baseUrl}signup`, body)
  }
  verifyOtp(body) {
    return this.http.post(`${this.baseUrl}verifyOtp`, body)
  }
  resetOpt(body) {
    return this.http.post(`${this.baseUrl}resendOtp`, body)
  }
  login(body) {
    return this.http.post(`${this.baseUrl}login`, body)
  }
  logout(){
    return this.http.get(`${this.baseUrl}logout`)
  }
  changePassword(body){
    return this.http.post(`${this.baseUrl}changePassword`, body)  
  }
  getUserDetail(){
    return this.http.get(`${this.baseUrl}viewProfile`)
 }
 getdata(){
  return this.http.get(`${this.baseUrl}webHomeScreen`)
 }
 getAuthdata(){
  return this.http.get(`${this.baseUrl}webHomeScreenAuth`)
 }
 getStaticData(){
  return this.http.get(`${this.baseUrl}getWebPages`)
 }
 Contactus(body) {
  return this.http.post(`${this.baseUrl}contactUs`, body)
}
ForgotPassword(body) {
  return this.http.post(`${this.baseUrl}forgetPassword`, body)
}
getProductData(id){
  return this.http.get(`${this.baseUrl}campaignDetail/${id}`)
}
getSettingData(){
  return this.http.get(`${this.baseUrl}loyaltySettings`)
}
getTicketData(page){
  return this.http.get(`${this.baseUrl}ticketScreen?page=${page}`)
}
getloyalityData(){
  return this.http.get(`${this.baseUrl}loyaltyPointScreen`)
}
getRedeemData(){
  return this.http.get(`${this.baseUrl}redeemLoyalty`)
}
AddRemoveFav(id){
  return this.http.get(`${this.baseUrl}addRemoveFav/${id}`)
}
AddFavguestUser(body){
  return this.http.post(`${this.baseUrl}addWishlist`,body)
}
Getwishlist(){
  return this.http.get(`${this.baseUrl}wishlist`)
}
addToCart(body){
  return this.http.post(`${this.baseUrl}updateCart`,body)
}
AddGuestUserCard(body){
  return this.http.post(`${this.baseUrl}addToCart`,body)
}
getCartData(){
  return this.http.get(`${this.baseUrl}cartInfo`)
}
getGuestUsercart(body){
  return this.http.post(`${this.baseUrl}guestCart`,body)
}
removeFromCart(id){
  return this.http.delete(`${this.baseUrl}removeFromCart/${id}`)
}
getGuestUserwishlist(body){
  return this.http.post(`${this.baseUrl}guestCart`,body)
}
addAddress(body){
  return this.http.post(`${this.baseUrl}addAddress`, body)  
}
getPaymenet(){
  return this.http.get(`${this.baseUrl}paymentScreen`)  
}
applyPromoCode(body){
return this.http.post(`${this.baseUrl}addCoupon`, body)  
}
order(body) {
  return this.http.post(`${this.baseUrl}order`, body)
}
resetPaasword(body){
  return this.http.post(`${this.baseUrl}forgetPasswordChange`, body)
}
profileUpdate(body){
  return this.http.post(`${this.baseUrl}editProfile`, body)
}
editProfileVerify(body){
  return this.http.post(`${this.baseUrl}editProfileVerify`, body)
}
}
