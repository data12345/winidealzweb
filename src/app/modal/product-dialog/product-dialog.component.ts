import { Component, OnInit, ViewChild } from '@angular/core';
import {MatDialog} from '@angular/material/dialog';

@Component({
  selector: 'app-product-dialog',
  templateUrl: './product-dialog.component.html',
  styleUrls: ['./product-dialog.component.scss']
})
export class ProductDialogComponent implements OnInit {
  selectedIndex: number;
  product;
  slideConfig = {};
  constructor(public dialog: MatDialog) {
  }

  ngOnInit() {
    this.slideConfig =  {"slidesToShow": 1,
    "slidesToScroll": 1,
    "infinite": true,
    "dots": false,
    "autoplay": false,
    "autoplaySpeed": 1000,
    "initialSlide": this.selectedIndex};

  }

  slickInit(event){
  }
}
