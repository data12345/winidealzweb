import { Component, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from 'src/app/services/api/api.service';
import { ToastrService } from 'ngx-toastr';
import * as _ from "lodash";
import { LocalStorage } from 'angular-web-storage';
import { CommonService } from 'src/app/services/common.service';
import { NgForm } from '@angular/forms';
declare var $: any;
import {MatDialog, MatDialogRef} from '@angular/material/dialog';

@Component({
  selector: 'app-address-dialog',
  templateUrl: './address-dialog.component.html',
  styleUrls: ['./address-dialog.component.scss']
})
export class AddressDialogComponent implements OnInit {
  order_name: any;
  order_number: any;
  postal_code: any;
  city: any;
  apartment: any;
  building_name: any;
  country_code_address: any;
  country: any;
  countryCode: any;
  countryStatusAddress: boolean = true;
  config1: {};
  userAddress: string;

  constructor(public dialogRef: MatDialogRef<AddressDialogComponent>,private router: Router, public service: ApiService, private toastr: ToastrService, public comm: CommonService) { }


  ngOnInit() {
    this.service.getCountryCode().subscribe((res: any) => {
      this.countryCode = res.countryArray
      console.log(this.countryCode.length, res.countryArray.length)
      this.config1 = {
        displayKey: "Code", //if objects array passed which key to be displayed defaults to description
        search: true, //true/false for the search functionlity defaults to false,
        height: "150px", //height of the list so that if there are more no of items it can show a scroll defaults to auto. With auto height scroll will never appear
        placeholder: "Code", // text to be displayed when no item is selected defaults to Select,
        customComparator: () => { }, // a custom function using which user wants to sort the items. default is undefined and Array.sort() will be used in that case,
        limitTo: this.countryCode.length, // a number thats limits the no of options displayed in the UI similar to angular's limitTo pipe
        moreText: "more", // text to be displayed whenmore than one items are selected like Option 1 + 5 more
        noResultsFound: "No results found!", // text to be displayed when no items are found while searching
        searchPlaceholder: "Search", // label thats displayed in search input,
        searchOnKey: "Code" // key on which search should be performed this will be selective search. if undefined this will be extensive search on all keys
      };
    });
    if (localStorage.getItem('sigUp')) {
      this.order_name = JSON.parse(localStorage.getItem('sigUp')).order_name
      this.order_number = JSON.parse(localStorage.getItem('sigUp')).order_number
      this.postal_code = JSON.parse(localStorage.getItem('sigUp')).postal_code
      this.city = JSON.parse(localStorage.getItem('sigUp')).city
      this.apartment = JSON.parse(localStorage.getItem('sigUp')).apartment
      this.building_name = JSON.parse(localStorage.getItem('sigUp')).building_name
      this.country_code_address = JSON.parse(localStorage.getItem('sigUp')).country_code
      this.country = JSON.parse(localStorage.getItem('sigUp')).country
      if(this.apartment=="" || this.city==''){
        this.userAddress=''
      }
      else{
      this.userAddress = this.city + '-' + this.postal_code + '\n' + this.building_name + '\n' +this.apartment 
      }
      console.log(this.userAddress)
    }
  }

  selectionChangedAddress(event) {
    if (event.value !=undefined) {
     this.countryStatusAddress = true;
     this.country_code_address = event.value.Code
    }
    else
    {
      this.countryStatusAddress = false;
      this.country_code_address=''
    }
  }

    updateAddress(form:NgForm){
    console.log(form,"datatat")
    let Data = new FormData();
    for (let key in form.value) {
      Data.append(`${key}`, form.value[key]);
      console.log(Data, `${key}`)
    }
    this.service.addAddress(Data).subscribe((res: any) => {
      if (res.status == 1) {
        this.toastr.success(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
        localStorage.setItem("sigUp", JSON.stringify(res.data));
        this.dialogRef.close(form.value);
      }
      else {
        this.toastr.error(res.uiMessage, "", { closeButton: true, easeTime: 700, timeOut: 1200, positionClass: 'toast-bottom-center' });
      }
    });
  }
}


