import { Component, OnInit } from '@angular/core';
import { ApiService } from '../services/api/api.service';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.component.html',
  styleUrls: ['./faq.component.scss']
})
export class FaqComponent implements OnInit {
  faq:any;

  constructor(private service: ApiService) { }
  ngOnInit() {
    window.scroll(0, 0);
    this.getContactusdata()
  }
  getContactusdata() {
    this.service.getStaticData().subscribe((res: any) => {
      console.log(res);
      if (res.status == 1) {
        this.faq=res.data.faq.data
      }
    }
    )
  }
}
