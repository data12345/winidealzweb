import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  selector: 'app-otpforgot',
  templateUrl: './otpforgot.component.html',
  styleUrls: ['./otpforgot.component.scss']
})
export class OtpforgotComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
}
goToresetpassword() {
  this.router.navigate(['/resetpassword'])
};
onOtpChange(event){
  
}

}
